<?php

use App\Product;
use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(Product::class, function (Faker $faker) {
    return [
       
        'sku' => $faker->uuid,
        'title' => $faker->text,
        'short_description' => $faker->sentence,
        'description' => $faker->sentence,
        'url_key' => $faker-> url,

        'cost'  => $faker->randomDigit,
        'price' => $faker->randomDigit,
        'special_price' => $faker->boolean,
        'special_price_from' => $faker->dateTime,
        'special_price_to' => $faker->dateTime,

        'stock' => $faker->randomDigit,            

        'weight' => $faker->randomDigit,
        'color' => $faker->colorName,             
        'size' => $faker->randomDigit,

        'is_new' => $faker->boolean,
        'is_featured' => $faker->boolean,
        'is_published' => $faker->boolean,
        'created_at' => Carbon::now()->toDateTimeString(),
        'updated_at' => Carbon::now()->toDateTimeString(),
       
    ];
});
